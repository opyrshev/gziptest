using GZipTest.Constants;
using GZipTest.Models;
using System;
using System.IO;

namespace GZipTest.Engine
{
    public class Compressor : AbstractEngine
    {
        public Compressor(Stream inputFileStream, Stream outputFileStream) : base(inputFileStream, outputFileStream)
        {
        }

        protected override void ReadFile()
        {
            var fileSize = InputFileStream.Length;
            try 
            {
                using (var binaryReader = new BinaryReader(InputFileStream))
                {
                    var index = 0;
                    while (fileSize > 0 && Error == null)
                    {
                        var currentBufferSize = fileSize > Const.BufferSize ? Const.BufferSize : fileSize;
                        var buffer = binaryReader.ReadBytes((int)currentBufferSize);

                        InputQueue.Enqueue(new BufferData(index++, buffer));
                        fileSize -= currentBufferSize;
                        if (fileSize == 0)
                        {
                            InputQueue.ReadComplete();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Error = e;
            }
            
        }

        protected override void Process(int processEventId)
        {
            try
            {
                while (InputQueue.Dequeue(out BufferData buffer) && Error == null)
                {
                    var compressPortion = GZip.GZip.CompressBuffer(buffer.Data);
                    if (compressPortion == null) throw new OutOfMemoryException();
                    OutputDictionary.Add(buffer.Index, compressPortion);
                }
                ProcessEvents[processEventId].Set();
            }
            catch (Exception e)
            {
                Error = e;
            }
        }

        protected override void WriteFile()
        {
            try
            {
                using (var binaryWriter = new BinaryWriter(OutputFileStream))
                {
                    while (OutputDictionary.GetValueByKey(out byte[] buffer) && Error == null)
                    {
                        binaryWriter.Write(buffer.Length);
                        binaryWriter.Write(buffer, 0, buffer.Length);
                    }
                }
            }
            catch (Exception e)
            {
                Error = e;
            }
        }
    }
}
