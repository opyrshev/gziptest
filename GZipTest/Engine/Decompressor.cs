﻿using GZipTest.Data;
using GZipTest.Helpers;
using GZipTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace GZipTest.Engine
{
    public class Decompressor : AbstractEngine
    {
        public Decompressor(Stream inputFileStream, Stream outputFileStream) : base(inputFileStream, outputFileStream)
        {
        }

        protected override void ReadFile()
        {
            try
            {
                using (var binaryReader = new BinaryReader(InputFileStream))
                {
                    var fileSize = InputFileStream.Length;

                    const int intSize = 4;
                    var bufferId = 0;

                    while (fileSize > 0 && Error == null)
                    {
                        var bufferSize = binaryReader.ReadInt32();
                        var buffer = binaryReader.ReadBytes(bufferSize);
                        InputQueue.Enqueue(new BufferData(bufferId++, buffer));
                        fileSize -= (bufferSize + intSize);
                        if (fileSize == 0)
                        {
                            InputQueue.ReadComplete();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Error = e;
            }
        }
        protected override void Process(int processEventId)
        {
            try
            {
                while (InputQueue.Dequeue(out BufferData buffer) && Error == null)
                {
                    var decompressedBuffer = GZip.GZip.DecompressBuffer(buffer.Data);
                    if (decompressedBuffer == null) throw new OutOfMemoryException();
                    OutputDictionary.Add(buffer.Index, decompressedBuffer);
                }
                ProcessEvents[processEventId].Set();
            }
            catch (Exception e)
            {
                Error = e;
            }
        }

        protected override void WriteFile()
        {
            try
            {
                using (var binaryWriter = new BinaryWriter(OutputFileStream))
                {
                    while (OutputDictionary.GetValueByKey(out var data) && Error == null)
                    {
                        binaryWriter.Write(data, 0, data.Length);
                    }
                }
            }
            catch (Exception e)
            {
                Error = e;
            }
        }
    }
}
        
