﻿using GZipTest.Data;
using GZipTest.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace GZipTest.Engine
{
    public abstract class AbstractEngine
    {
        protected AutoResetEvent[] ProcessEvents = new AutoResetEvent[EnvMetrics.ProcessorCount()];

        protected Stream InputFileStream { get; set; }

        protected Stream OutputFileStream { get; set; }

        protected Exception Error { get; set; }

        protected Reader InputQueue { get; set; }

        protected Writer OutputDictionary { get; set; }

        public AbstractEngine(Stream inputFileStream, Stream outputFileStream)
        {
            InputFileStream = inputFileStream;
            OutputFileStream = outputFileStream;
            InputQueue = new Reader();
            OutputDictionary = new Writer();
        }

        public int Run()
        {
            var readingThread = new Thread(new ThreadStart(ReadFile));
            var compressingThreads = new List<Thread>();
            for (var i = 0; i < EnvMetrics.ProcessorCount(); i++)
            {
                var j = i;
                ProcessEvents[j] = new AutoResetEvent(false);
                compressingThreads.Add(new Thread(() => Process(j)));
            }
            var writingThread = new Thread(new ThreadStart(WriteFile));

            readingThread.Start();

            foreach (var compressThread in compressingThreads)
            {
                compressThread.Start();
            }

            writingThread.Start();

            WaitHandle.WaitAll(ProcessEvents);
            OutputDictionary.SetCompleted();

            writingThread.Join();
            if (Error != null)
            {
                Console.WriteLine(Error.Message);
                return 1;
            }
            else
            {
                Console.WriteLine("Complete");
                return 0;
            }
        }

        protected abstract void ReadFile();

        protected abstract void Process(int processEventId);

        protected abstract void WriteFile();
    }
}
