using GZipTest.Engine;
using System;
using System.IO;

namespace GZipTest
{
    public class Program
    {
        public static int Main(string[] args)
        {
            if (Helpers.ArgValidator.ValidateInputArgs(args))
            {
                var isCompress = (args[0].ToLower() == "compress");

                var sourceFile = args[1];

                var targetFile = (args[0].ToLower() == "compress" && !args[2].ToLower().Contains(".gz")) ? args[2] + ".gz" : args[2];

                using (var sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
                using (var targetStream = new FileStream(targetFile, FileMode.Create, FileAccess.Write))
                {
                    try
                    {
                        AbstractEngine zipper; 
                        if (isCompress)
                        {
                            zipper = new Compressor(sourceStream, targetStream);
                        }
                        else
                        {
                            zipper = new Decompressor(sourceStream, targetStream);
                        }
                        return zipper.Run();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return 1;
                    }
                }
            }
            else
            {
                return 1;
            }
        }
    }
}
