﻿namespace GZipTest.Constants
{
    public class Const
    {
        public static readonly int BufferSize = 1024 * 1024;
    }
}
