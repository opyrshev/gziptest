using System.Collections.Generic;

namespace GZipTest.Models
{
    public class BufferData
    {
        public byte[] Data;

        public int Index;

        public BufferData(int _index, byte[] _data)
        {
            Data = _data;
            Index = _index;
        }

        public BufferData()
        { }
    }
}
