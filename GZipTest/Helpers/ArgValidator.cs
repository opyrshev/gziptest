﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GZipTest.Helpers
{
    public class ArgValidator
    {
        public static bool ValidateInputArgs(string[] args)
        {
            if (args?.Length != 3)
            {
                Console.WriteLine("Incorrect number of parameters");
                GetInfo();
                return false;
            }

            if (args[0].ToLower() != "compress" && args[0].ToLower() != "decompress")
            {
                Console.WriteLine("Invalid first argument choose of [compress/decompress]");
                GetInfo();
                return false;
            }

            var sourceFile = new FileInfo(args[1]);

            var targetFile = new FileInfo((args[0].ToLower() == "compress" && !args[2].ToLower().Contains(".gz")) ?  args[2] + ".gz" : args[2]);

            if (!sourceFile.Exists || sourceFile.Length == 0)
            {
                Console.WriteLine($"File {sourceFile.FullName} does not exist");
                return false;
            }

            if (targetFile.Exists)
            {
                Console.WriteLine($"File {targetFile.FullName} exists");
                return false;
            }

            return true;
        }

        private static void GetInfo()
        {
            Console.WriteLine("Please use follow command format:");
            Console.WriteLine("GZipTest.exe compress/decompress [source file] [target file]");
        }
    }
}
