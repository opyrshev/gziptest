using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace GZipTest.Helpers
{
    public static class EnvMetrics
    {
        public static Memory GetMemory()
        {
            var output = "";
            var info = new ProcessStartInfo();

            info.FileName = "wmic";
            info.Arguments = "OS get FreePhysicalMemory,TotalVisibleMemorySize /Value";
            info.RedirectStandardOutput = true;

            using (var process = Process.Start(info))
            {
                output = process.StandardOutput.ReadToEnd();
            }

            var lines = output.Trim().Split("\n");
            var freeMemoryPart = lines[0].Split("=", StringSplitOptions.RemoveEmptyEntries)[1];
            var totalMemoryPart = lines[1].Split("=", StringSplitOptions.RemoveEmptyEntries)[1];

            var metrics = new Memory();
            metrics.Free = double.Parse(freeMemoryPart);
            metrics.Total = double.Parse(totalMemoryPart);
            return metrics;
        }

        public static int ProcessorCount()
        {
            return Environment.ProcessorCount;
        }

        public static DriveInfo FreeSpace(string path)
        {
            var dirInfo = new FileInfo(path).Directory.Root;
            var drive = DriveInfo.GetDrives().Where(x => x.Name == dirInfo.Name).FirstOrDefault();
            return drive;
        }
    }
}
