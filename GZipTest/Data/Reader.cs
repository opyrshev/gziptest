﻿using GZipTest.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace GZipTest.Data
{
    public class Reader
    {
        private readonly object _locker = new object();
        private Queue<BufferData> _queue = new Queue<BufferData>();
        private bool _complete = false;

        public void Enqueue(BufferData data)
        {
            lock (_locker)
            {
                _queue.Enqueue(data);
                Monitor.PulseAll(_locker);
            }
        }

        public bool Dequeue(out BufferData data)
        {
            lock (_locker)
            {
                while (_queue.Count == 0)
                {
                    if (_complete)
                    {
                        data = new BufferData();
                        return false;
                    }

                    Monitor.Wait(_locker);
                }
                data = _queue.Dequeue();
                Monitor.PulseAll(_locker);
                return true;
            }
        }

        public void ReadComplete()
        {
            lock (_locker)
            {
                _complete = true;
                Monitor.PulseAll(_locker);
            }
        }
    }
}
