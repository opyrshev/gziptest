﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using GZipTest.Constants;
using System.Text;

namespace GZipTest.GZip
{
    public class GZip
    {
        public static byte[] CompressBuffer(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var compressStream = new GZipStream(output, CompressionMode.Compress))
                {
                    compressStream.Write(data, 0, data.Length);
                }

                return output.ToArray();
            }
        }

        public static byte[] DecompressBuffer(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var input = new MemoryStream(data))
                {
                    using (var decompressStream = new GZipStream(input, CompressionMode.Decompress))
                    {
                        var buffer = new byte[Const.BufferSize];
                        int bytesRead;

                        while ((bytesRead = decompressStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                        }
                    }

                    return output.ToArray();
                }
            }
        }
    }
}
